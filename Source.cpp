#include <iostream>

class Animal
{
	std::string AnimalSound = "AnimalVoice";
public:
	virtual void Voice() 
	{
		std::cout << AnimalSound;
	}
};

class Cow :public Animal
{
	std::string CowSound = "Moo";
public:
	void Voice() override 
	{
		std::cout << CowSound;
	}
};

class Dog :public  Animal
{
	std::string DogSound = "Woof";
public:
	void Voice() override
	{
		std::cout << DogSound;
	}
};

class Cat :public  Animal
{
	std::string CatSound = "Meow";
public:
	void Voice() override 
	{
		std::cout << CatSound;
	}
};

int main()
{
	Animal* AnimCall[3];
	AnimCall[0] = new Cow;
	AnimCall[1] = new Dog;
	AnimCall[2] = new Cat;

	for (int i=0; i<3;i++) 
	{
		AnimCall[i]->Voice();
		std::cout << '\n';
	}
}